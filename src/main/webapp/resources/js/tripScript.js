/**
 * 
 */
function validateTrip(){
	event.preventDefault();
	var arr = document.getElementsByTagName('input');
	var destination = arr[0].value;
	var departureTime = arr[1].value;
	var driver = arr[2].value;
	var carType = arr[3].value;
	var maxTicket = arr[4].value;
	var departureDate = arr[5].value;
	
	if(destination == ""||departureTime==""||driver==""||carType==""||departureDate==""){
		alert("Please fill all field");
		return false;
	}
	if(isNaN(maxTicket)){
		alert("Maximum Online Ticket must be a number");
		return false;
	}
	alert("Add successful");
	document.tripForm.submit();
}

function confirmDelete(){
	var choice = confirm("Do you want to delete?");
	if(choice == 1){
		alert("Delete successful!");
		return;
	}else{
		event.preventDefault();
		return;
	}
}