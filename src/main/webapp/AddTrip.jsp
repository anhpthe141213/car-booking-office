<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Car Park</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <!-- Validation -->
    <script src="resources/js/tripScript.js"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><i class="fa fa-plane" aria-hidden="true"></i> Trip</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="divider"></li>
                    <li><a href="#">Welcome Nguyễn Văn A</a>
                </li>
                
                <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="active">
                            <a href="#"><i class="fa fa-plane"></i> Trip manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="TripList.jsp"><i class="fa fa-list"></i> Trip list</a>
                                </li>
                                <li>
                                    <a href="AddTrip.jsp"><i class="fa fa-plus"></i> Add Trip</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-ticket"></i> Ticket manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="ticket-list.html"><i class="fa fa-list"></i> Ticket list</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Add Trip</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            <form name="tripForm" action="AddTripServlet" method="post" onsubmit="return validateTrip()">
                
                <div class="row pt-15">
                    <label for="destination" class="col-lg-2 col-form-label">Destination <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="destination" type="text" placeholder="Enter destination" required="required" maxlength="50" name="destination">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="departure_time" class="col-lg-2 col-form-label">Departure time <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="departure_time" type="time" placeholder="Enter departure time" required="required" name="departureTime">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="driver" class="col-lg-2 col-form-label">Driver <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="driver" type="text" placeholder="Enter driver" required="required" maxlength="50" name="driver">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="car_type" class="col-lg-2 col-form-label">Car type <span class="red">(*)</span></label>
                    <div class="col-lg-3">
                      <input class="form-control" id="car_type" type="text" placeholder="Enter car type" required="required" maxlength="50" name="carType">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="maximum_online_ticket_number" class="col-lg-2 col-form-label">Maximum online ticket number <span class="red">(*)</span></label>
                    <div class="col-lg-3">
                      <input class="form-control" id="maximum_online_ticket_number" type="number" placeholder="Enter maximum online ticket number" required="required" name="maxTicket">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="departure_date" class="col-lg-2 col-form-label">Departure date <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="departure_date" type="date" placeholder="departure_date" required="required" name="departureDate">
                    </div>
                </div>

                <div class="form-group pt-15">
                  <div class="form-row">
                      <div class="col-md-5" align="center">
                            <button type="reset" class="btn btn-warning"><i class="fa fa-undo"></i> Reset</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                      </div>
                  </div>
                </div>

            </form>

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>