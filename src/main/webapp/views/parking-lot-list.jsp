<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Car Park</title>

<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link
	href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css"
	rel="stylesheet">
<link href="<%=request.getContextPath()%>/resources/css/style.css"
	rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<%=request.getContextPath()%>/TripList.jsp"><i class="fa fa-map-marker"
					aria-hidden="true"></i> Parking lot</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">

				<li class="divider"></li>
				<li><a href="#">Welcome Nguyen Van A</a></li>

				<li class="divider"></li>
				<li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="#"><i class="fa fa-dashboard fa-fw"></i> Car
								park manager</a></li>
						<li><a href="#"><i class="fa fa-car"></i> Car manager<span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="car-list.html"><i class="fa fa-list"></i>
										Car list</a></li>
								<li><a href="car-add.html"><i class="fa fa-plus"></i>
										Add Car</a></li>
							</ul></li>
						<li><a href="#"><i class="fa fa-cart-plus"></i> Booking
								office manager<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="booking-office-list.html"><i
										class="fa fa-list"></i> Booking office list</a></li>
								<li><a href="booking-office-add.html"><i
										class="fa fa-plus"></i> Add Booking office</a></li>
							</ul></li>
						<li class="active"><a href="#"><i
								class="fa fa-map-marker"></i> Parking lot manager<span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
                                    <a href="<%=request.getContextPath()%>/ParkingLotList"><i class="fa fa-list"></i> Parking lot list</a>
                                </li>
                                <li>
                                    <a class="active" href="<%=request.getContextPath()%>/add"><i class="fa fa-plus"></i> Add Parking lot</a>
                                </li>
							</ul></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->

		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Parking lot list</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<form action="<%=request.getContextPath()%>/search" method="post">
						<div class="col-lg-5"></div>
						<div class="col-lg-3 input-group">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" name="searchString" class="form-control"
								placeholder="Parking lot Search">
						</div>

						<div class="col-lg-3 input-group">
							<span class="input-group-addon"><i class="fa fa-filter"></i>
								Filter By</span> 
								<select class="form-control" name="filterBy">
									<option value="parkPlace">Place</option>
									<option value="parkStatus">Status</option>
							</select>
						</div>

						<div class="col-lg-1 input-group">
							<button type="submit" name="1" class="btn btn-info">Search</button>
						</div>
					</form>
				</div>

				<div class="table-responsive pt-15">
					<table class="table table-hover table-striped table-bordered">
						<thead class="table-dark">
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Parking lot</th>
								<th scope="col">Place</th>
								<th scope="col">Area</th>
								<th scope="col">Price (VND)</th>
								<th scope="col">Status</th>
								<th scope="col" colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="l">
								<tr>
									<td scope="row">${l.park_id}</td>
									<td scope="row">${l.parking_lot_name}</td>
									<td scope="row">${l.place}</td>
									<td scope="row">${l.area}</td>
									<td scope="row">${l.price}</td>
									<td scope="row">${l.status}</td>
									<td scope="row"><a
										href="actionController?action=edit&id=${l.park_id}"
										class="fa fa-pencil-square-o"> Edit</a></td>
									<td><a
										href="actionController?action=delete&id=${l.park_id}"
										class="fa fa-trash pl-10"> Delete</a></td>
								</tr>
							</c:forEach>
							<!--  <tr>
								<td scope="row">1</td>
								<td scope="row"><a href="#">Bai so 1</a></td>
								<td scope="row">Khu Tay</td>
								<td scope="row">10 m2</td>
								<td scope="row">2500</td>
								<td scope="row">Blank</td>
								<td scope="row"><a href="#"><i
										class="fa fa-pencil-square-o"></i> Edit</a><a href="#"
									class="pl-10"><i class="fa fa-trash"></i> Delete</a></td>
							</tr>-->
						</tbody>
					</table>
				</div>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Previous</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>
			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script
		src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

</body>
</html>