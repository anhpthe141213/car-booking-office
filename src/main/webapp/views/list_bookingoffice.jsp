<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Car Park</title>

<!-- Bootstrap Core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="dist/css/sb-admin-2.css" rel="stylesheet">
<link href="dist/css/style.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
</head>
<body>


	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<%=request.getContextPath()%>/TripList.jsp"><i class="fa fa-cart-plus"
					aria-hidden="true"></i> Booking office</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">

				<li class="divider"></li>
				<li><a href="#">Welcome Lê Văn Đan</a></li>

				<li class="divider"></li>
				<li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="#"><i class="fa fa-dashboard fa-fw"></i> Car
								park manager</a></li>
						<li><a href="#"><i class="fa fa-car"></i> Car manager<span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="car-list.html"><i class="fa fa-list"></i>
										Car list</a></li>
								<li><a href="car-add.html"><i class="fa fa-plus"></i>
										Add Car</a></li>
							</ul></li>
						<li class="active"><a href="#"><i class="fa fa-cart-plus"></i>
								Booking office manager<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a class="active" href="<%=request.getContextPath()%>/CallListBookingofficePageServlet"><i
										class="fa fa-list"></i> Booking office list</a></li>
								<li><a href="booking-office-add.html"><i
										class="fa fa-plus"></i> Add Booking office</a></li>
							</ul></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> Parking
								lot manager<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li><a href="parking-lot-list.html"><i
										class="fa fa-list"></i> Parking lot list</a></li>
								<li><a href="parking-lot-add.html"><i
										class="fa fa-plus"></i> Add Parking lot</a></li>
							</ul></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->

		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Booking office list</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<form>
						<div class="col-lg-5"></div>
						<div class="col-lg-4 input-group" style="margin-left: -50px">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" id="search" name="search" class="form-control"
								placeholder="Booking office Search">
						</div>

						<div class="col-lg-2 input-group">
							<span class="input-group-addon"><i class="fa fa-filter"></i>
								Filter By</span> <select id="order" class="form-control" style="width: 90px">
								<option value="officeName">Name</option>
								<option value="destination">Trip</option>
							</select>
						</div>

						<div class="col-lg-1 input-group">
							<button type="button" onclick="s()" name="1" class="btn btn-info">Search</button>
						</div>
					</form>
				</div>

				<div class="list" name="list" id="list">
				

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	</div>
	<!-- /#wrapper -->




	<!-- jQuery -->
	
	
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script>
	var check=0;
	load()
	function load(){
		$.ajax({
			  url: "<%=request.getContextPath()%>/ListBookingofficeServlet",
			  type: "get",
			  success: function(data){
				  var list = document.getElementById("list");
				  list.innerHTML = data;	  
				  }
			});
	}
	function loadd(i){
		
		var searchString = "";
		
		if(check==1){
			searchString = document.getElementById("search").value;
		}
		
		var order = document.getElementById("order").value;
		
		$.ajax({
			  url: "<%=request.getContextPath()%>/ListBookingofficeServlet?pageIndex=" + i + "&order=" + order + "&searchString=" + searchString,
						type : "get",
						success : function(data) {
							var list = document.getElementById("list");
							list.innerHTML = data;
						}
					});
		}
		function s() {
			if (document.getElementById("search").value != "") {
				check = 1;
				loadd(1);
			} else {
				window.alert("Please input data to search field!")
			}
		}

		function home() {
			window.location.reload()
		}
	</script>
</body>
</html>