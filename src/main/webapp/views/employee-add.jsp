<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Car Park</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/resources/css/sb-admin-2.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<%=request.getContextPath()%>/TripList.jsp"><i class="fa fa-users" aria-hidden="true"></i> Employee</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="divider"></li>
                    <li><a href="#">Welcome Anhpthe50</a>
                </li>
                
                <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Employee manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<%=request.getContextPath()%>/ListEmployeeController"><i class="fa fa-list"></i> Employee list</a>
                                </li>
                                <li>
                                    <a class="active" href="<%=request.getContextPath()%>/AddEmployeeController"><i class="fa fa-plus"></i> Add Employee</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Add Employee</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            <form action="<%=request.getContextPath()%>/AddEmployeeController" method="post">
                
                <div class="row pt-15">
                    <label for="full_name" class="col-lg-2 col-form-label">Full name <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="full_name" type="text" placeholder="Enter full name" required="required" maxlength="50" name="fullname">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="phone_number" class="col-lg-2 col-form-label">Phone number <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="phone_number" type="text" placeholder="Enter phone number" required="required" maxlength="11" name="phonenumber">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="birth_date" class="col-lg-2 col-form-label">Date of birth <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="birth_date" type="date" placeholder="Enter date of birth" required="required" name="dateofbirth">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="sex" class="col-lg-2 col-form-label">Sex <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                        <input class="form-check-input" type="radio" name="gender" id="male" value="0" checked>
                        <label class="form-check-label" for="male">Male</label>
                        <input class="form-check-input" type="radio" name="gender" id="female" value="1">
                        <label class="form-check-label" for="female">Female</label>
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="address" class="col-lg-2 col-form-label">Address</label>
                    <div class="col-lg-3">
                      <input class="form-control" id="address" type="text" placeholder="Enter address" required="required" maxlength="50" name="address">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="phone_number" class="col-lg-2 col-form-label">Email</label>
                    <div class="col-lg-3">
                      <input class="form-control" id="phone_number" type="text" placeholder="Enter phone number" required="required" maxlength="50" name="email">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="account" class="col-lg-2 col-form-label">Account <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="account" type="text" placeholder="Enter account" required="required" minlength="5" maxlength="50" name="account">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="Password" class="col-lg-2 col-form-label">Password <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="Password" type="password" placeholder="Enter Password" required="required" minlength="8" maxlength="50" name="pass">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="department" class="col-lg-2 col-form-label">Department <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <select class="form-control" id="department" type="text" required="required" name="department">
                      <c:forEach items="${requestScope.listEmployee}" var="employee">
                      		<option>${employee.department}</option>
                      </c:forEach>
                      </select>
                    </div>
                </div>

                <div class="form-group pt-30">
                  <div class="form-row">
                      <div class="col-lg-5" align="center">
                            <button type="button" class="btn btn-info"><i class="fa fa-backward"></i> Back</button>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-undo"></i> Reset</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add</button>
                      </div>
                  </div>
                </div>

            </form>

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
