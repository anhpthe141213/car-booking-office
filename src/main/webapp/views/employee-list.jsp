<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Car Park</title>

<!-- Bootstrap Core CSS -->
<link
	href="<%=request.getContextPath()%>/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">

<!-- MetisMenu CSS -->
<link
	href="<%=request.getContextPath()%>/vendor/metisMenu/metisMenu.min.css"
	rel="stylesheet">

<!-- Custom CSS -->
<link href="<%=request.getContextPath()%>/resources/css/sb-admin-2.css"
	rel="stylesheet">
<link href="style.css" rel="stylesheet">

<!-- Custom Fonts -->
<link
	href="<%=request.getContextPath()%>/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
	

</head>

<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation">

			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<%=request.getContextPath()%>/TripList.jsp"><i class="fa fa-users"
					aria-hidden="true"></i> Employee</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">

				<li class="divider"></li>
				<li><a href="#">Welcome Anhpt50</a></li>

				<li class="divider"></li>
				<li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
				</li>
			</ul>
			<!-- /.navbar-top-links -->

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse">
					<ul class="nav" id="side-menu">
						<li><a href="#"><i class="fa fa-dashboard fa-fw"></i>
								Dashboard</a></li>
						<li class="active"><a href="#"><i
								class="fa fa-bar-chart-o fa-fw"></i> Employee manager<span
								class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
                                    <a href="<%=request.getContextPath()%>/ListEmployeeController"><i class="fa fa-list"></i> Employee list</a>
                                </li>
                                <li>
                                    <a class="active" href="<%=request.getContextPath()%>/AddEmployeeController"><i class="fa fa-plus"></i> Add Employee</a>
                                </li>
							</ul></li>
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->

		</nav>

		<!-- Page Content -->
		<div id="page-wrapper">
			<div class="container-fluid">

				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">Employee list</h1>
					</div>
					<!-- /.col-lg-12 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<form>
						<div class="col-lg-5"></div>
						<div class="col-lg-4 input-group">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" name="1" class="form-control"
								placeholder="User Search">
						</div>

						<div class="col-lg-2 input-group">
							<span class="input-group-addon"><i class="fa fa-filter"></i>
								Filter By</span> <select class="form-control">
								<option value="name">Name</option>
								<option value="department">Department</option>
							</select>
						</div>

						<div class="col-lg-1 input-group">
							<button type="button" name="1" class="btn btn-info">Search</button>
						</div>
					</form>
				</div>

				<div class="table-responsive pt-15">
					<table class="table table-hover table-striped table-bordered">
						<thead class="table-dark">
							<tr>
								<th scope="col">ID</th>
								<th scope="col">Name</th>
								<th scope="col">Date of birth</th>
								<th scope="col">Address</th>
								<th scope="col">Phone number</th>
								<th scope="col">Department</th>
								<th scope="col">Action</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${requestScope.listEmployee}" var="employee">
								<tr>
									<td scope="row">${employee.employeeId}</td>
									<td scope="row">${employee.employeeName}</td>
									<td scope="row">${employee.employeeBirthdate}</td>
									<td scope="row">${employee.employeeAddress}</td>
									<td scope="row">${employee.employeePhone}</td>
									<td scope="row">${employee.department}</td>
									<td scope="row"><a href="<%=request.getContextPath()%>/DetailEmployeeController?employeeId=${employee.employeeId}"><i class="fa fa-eye"></i>
											View</a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<nav>
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Previous</a></li>
						<li class="page-item active"><a class="page-link" href="#">1</a></li>
						<li class="page-item"><a class="page-link" href="#">2</a></li>
						<li class="page-item"><a class="page-link" href="#">3</a></li>
						<li class="page-item"><a class="page-link" href="#">Next</a></li>
					</ul>
				</nav>

			</div>
			<!-- /.container-fluid -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="vendor/jquery/jquery.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="vendor/metisMenu/metisMenu.min.js"></script>

	<!-- Custom Theme JavaScript -->
	<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
