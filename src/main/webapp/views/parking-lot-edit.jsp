<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Car Park</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%=request.getContextPath()%>/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%=request.getContextPath()%>/resources/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<%=request.getContextPath()%>/resources/css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<%=request.getContextPath()%>/resources/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<%=request.getContextPath()%>/TripList.jsp"><i class="fa fa-map-marker" aria-hidden="true"></i> Parking lot</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="divider"></li>
                    <li><a href="#">Welcome Nguyen Van A</a>
                </li>
                
                <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Car park manager</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-car"></i> Car manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="car-list.html"><i class="fa fa-list"></i> Car list</a>
                                </li>
                                <li>
                                    <a href="car-add.html"><i class="fa fa-plus"></i> Add Car</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cart-plus"></i> Booking office manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="booking-office-list.html"><i class="fa fa-list"></i> Booking office list</a>
                                </li>
                                <li>
                                    <a href="booking-office-add.html"><i class="fa fa-plus"></i> Add Booking office</a>
                                </li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#"><i class="fa fa-map-marker"></i> Parking lot manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<%=request.getContextPath()%>/ParkingLotList"><i class="fa fa-list"></i> Parking lot list</a>
                                </li>
                                <li>
                                    <a class="active" href="<%=request.getContextPath()%>/add"><i class="fa fa-plus"></i> Add Parking lot</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Edit Parking lot</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

            <form action="<%=request.getContextPath()%>/update" method="post">
                
                <div class="row pt-15">
                    <label for="parking_lot" class="col-lg-2 col-form-label">Parking lot <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="parking_lot" type="text" name="parkingLotName" value="${parkinglot.parking_lot_name}" placeholder="Enter parking lot" required="required" maxlength="50">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="palce" class="col-lg-2 col-form-label">Place <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <select class="form-control" id="palce" name="place" required="required">
	                      <option value="1">Khu Dong</option>
	                      <option value="2">Khu Tay</option>
	                      <option value="3">Khu Nam</option>
	                      <option value="4">Khu Bac</option>
                      </select>
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="area" class="col-lg-2 col-form-label">Area <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="area" type="text" name="area" value="${parkinglot.area}" placeholder="Enter area" required="required" maxlength="50">
                    </div>
                </div>

                <div class="row pt-15">
                    <label for="price" class="col-lg-2 col-form-label">Price <span class="red">(*)</span> </label>
                    <div class="col-lg-3">
                      <input class="form-control" id="price" type="text" name="price" value="${parkinglot.price}" placeholder="Enter price" required="required" maxlength="10">
                    </div>
                    <label class="col-lg-1 pt-5">(VND)</label>
                </div>

                <div class="form-group pt-30">
                  <div class="form-row">
                      <div class="col-md-5" align="center">
                            <button type="reset" class="btn btn-warning"><i class="fa fa-undo"></i> Reset</button>
                            <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Edit</button>
                      </div>
                  </div>
                </div>
            </form>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<%=request.getContextPath()%>/resources/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<%=request.getContextPath()%>/resources/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<%=request.getContextPath()%>/resources/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<%=request.getContextPath()%>/resources/dist/js/sb-admin-2.js"></script>

</body>
</html>
