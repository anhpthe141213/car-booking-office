<%@page import="dao.TripDao"%>
<%@page import="entities.Trip"%>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Car Park</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>
<body>

    <div id="wrapper">

        <!-- Navigation -->
                <nav class="navbar navbar-default navbar-static-top" role="navigation">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="TripList.jsp"><i class="fa fa-plane" aria-hidden="true"></i> Trip</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="divider"></li>
                    <li><a href="#">Welcome UserName</a>
                </li>
                
                <li class="divider"></li>
                    <li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="active">
                            <a href="#"><i class="fa fa-plane"></i> Trip manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a class="active" href="TripList.jsp"><i class="fa fa-list"></i> Trip list</a>
                                </li>
                                <li>
                                    <a href="AddTrip.jsp"><i class="fa fa-plus"></i> Add Trip</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-ticket"></i> Ticket manager<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="TicketList.jsp"><i class="fa fa-list"></i> Ticket list</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->

        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Trip list</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                
                <div class="row">
                    <form>
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4 input-group">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            <input type="text" name="searchBar" id="searchBar" class="form-control" placeholder="User Search">
                        </div>
                        <div class="col-lg-1 input-group">
                            <button type="button" name="searchBtn" class="btn btn-info" onclick="searchData()">Search</button>
                        </div>

                    </form>
                </div>

                <div class="table-responsive pt-15">    
                    <table class="table table-hover table-striped table-bordered">
                        <thead class="table-dark">
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Destination</th>
                                <th scope="col">Departure time</th>
                                <th scope="col">Driver</th>
                                <th scope="col">Car type</th>
                                <th scope="col">Booked ticket number</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="content">
                        
                    	<!-- /.Where contents go -->
                    	
                        </tbody>
                    </table>
                </div>
                
                
                <!-- /.PAGING -->
                    <nav>
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>


            </div>
            <p></p>
            <a href="views/parking-lot-add.jsp">Click here to Go to add parking lot</a><br>
            <a href="views/employee-add.jsp">Click here to Go to add Employee</a><br>
            <a href="CallListBookingofficePageServlet">Click here to Go to ListBookingOffice</a>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- Validation -->
    <script src="resources/js/tripScript.js"></script>
    
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	function loadData() {
	$.ajax({
		  url: "/MP_With_Front_End/TripListServlet",
		  type: "get", 
		  
		  success: function(data) {
		    var row = document.getElementById("content");
		    row.innerHTML+=data;
		  },
		  error: function(xhr) {
		    //Do Something to handle error
		  }
		});
}
	function searchData(){
		var searchWord = document.getElementById("searchBar").value;
		$.ajax({
		  url: "/MP_With_Front_End/SearchTripServlet",
		  type: "post",
		  data: {
			  searchW: searchWord
		  },
		  success: function(data) {
			  var row = document.getElementById("content");
			  row.innerHTML=data;
		  },
		  error: function(xhr) {
		    //Do Something to handle error
		  }
		});
	}
	
	window.onload = loadData();
	
</script>
</html>