package entities;

import java.util.Date;

public class Trip {
	/**
	 * [tripId] BIGINT PRIMARY KEY IDENTITY(1,1),
		[bookedTicketNumber] INT DEFAULT 0,
		[carType] VARCHAR(11) NOT NULL,
		[departureDate] DATE NOT NULL,
		[departureTime] TIME NOT NULL,
		[destination] VARCHAR(50),
		[driver] VARCHAR(11) NOT NULL,
		[maximumOnlineTicketNumber] INT NOT NULL
	 */
	private int tripId;
	private int bookedTicket;
	private String carType;
	private Date departureDate;
	private Object departureTime;
	private String destination;
	private String driver;
	private int maxTicket;
	public Trip() {
		super();
	}
	public Trip(int tripId, int bookedTicket, String carType, Date departureDate, Object departureTime,
			String destination, String driver, int maxTicket) {
		super();
		this.tripId = tripId;
		this.bookedTicket = bookedTicket;
		this.carType = carType;
		this.departureDate = departureDate;
		this.departureTime = departureTime;
		this.destination = destination;
		this.driver = driver;
		this.maxTicket = maxTicket;
	}
	public int getTripId() {
		return tripId;
	}
	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	public int getBookedTicket() {
		return bookedTicket;
	}
	public void setBookedTicket(int bookedTicket) {
		this.bookedTicket = bookedTicket;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public Date getDepartureDate() {
		return departureDate;
	}
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}
	public Object getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Object departureTime) {
		this.departureTime = departureTime;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getDriver() {
		return driver;
	}
	public void setDriver(String driver) {
		this.driver = driver;
	}
	public int getMaxTicket() {
		return maxTicket;
	}
	public void setMaxTicket(int maxTicket) {
		this.maxTicket = maxTicket;
	}
	
	
}
