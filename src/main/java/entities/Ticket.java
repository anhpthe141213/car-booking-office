package entities;

public class Ticket {
	private int ticketId;
	private String bookingTime;
	private String customerName;
	private String lincensePlate;
	private int tripId;

	public Ticket() {
		super();
	}

	public Ticket(int ticketId, String bookingTime, String customerName, String lincensePlate, int tripId) {
		super();
		this.ticketId = ticketId;
		this.bookingTime = bookingTime;
		this.customerName = customerName;
		this.lincensePlate = lincensePlate;
		this.tripId = tripId;
	}

	public int getTicketId() {
		return ticketId;
	}

	public void setTicketId(int ticketId) {
		this.ticketId = ticketId;
	}

	public String getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(String bookingTime) {
		this.bookingTime = bookingTime;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getLincensePlate() {
		return lincensePlate;
	}

	public void setLincensePlate(String lincensePlate) {
		this.lincensePlate = lincensePlate;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

}
