package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import entities.Trip;

public class TripDao extends BaseDao<Trip>{
	
	//Get all data
	@Override
	public ArrayList<Trip> getAll() {
		ArrayList<Trip> trips = new ArrayList<Trip>();
		try {
			String sql = "SELECT [tripId],[bookedTicketNumber],[carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber] FROM Trip";
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Trip t = new Trip();
				t.setTripId(rs.getInt("tripId"));
				t.setBookedTicket(rs.getInt("bookedTicketNumber"));
				t.setCarType(rs.getString("carType"));
				t.setDepartureDate(rs.getDate("departureDate"));
				t.setDepartureTime(rs.getTime("departureTime"));
				t.setDestination(rs.getString("destination"));
				t.setDriver(rs.getString("driver"));
				t.setMaxTicket(rs.getInt("maximumOnlineTicketNumber"));
				trips.add(t);
			}
		} catch (SQLException ex) {
			Logger.getLogger(TripDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		return trips;
	}
	
	//Add Trip	
	String addTrip = "INSERT INTO Trip([carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber]) VALUES (?,?,?,?,?,?)";

	public void create(String carType, String departureDate, String departureTime, String destination, String driver, int maxTicket) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(addTrip);
		preparedStatement.setString(1, carType);
		preparedStatement.setString(2, departureDate);
		preparedStatement.setString(3, departureTime);
		preparedStatement.setString(4, destination);
		preparedStatement.setString(5, driver);
		preparedStatement.setInt(6, maxTicket);
		preparedStatement.executeUpdate();
	}
	
	//Delete Trip	
	String deleteTrip = "DELETE FROM Trip WHERE [tripId] = ?";

	public void delete(int id) throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(deleteTrip);
		preparedStatement.setInt(1, id);
		preparedStatement.executeUpdate();
	}

	//Update Trip
	String updateTrip = "UPDATE Trip SET [carType] = ?,[departureDate] = ?,[departureTime] = ?,[destination] =?,[driver]=?,[maximumOnlineTicketNumber] = ? "
			+ "WHERE [tripId] = ?";
	public void update(String carType, String departureDate, String departureTime, String destination, String driver, int maxTicket, int id) 
			throws SQLException {
		PreparedStatement preparedStatement = connection.prepareStatement(updateTrip);
		preparedStatement.setString(1, carType);
		preparedStatement.setString(2, departureDate);
		preparedStatement.setString(3, departureTime);
		preparedStatement.setString(4, destination);
		preparedStatement.setString(5, driver);
		preparedStatement.setInt(6, maxTicket);
		preparedStatement.setInt(7, id);
		preparedStatement.executeUpdate();
	}
	
	//Get trip by ID
	String getById = "SELECT [tripId],[bookedTicketNumber],[carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber] "
			+ "FROM Trip WHERE [tripId] = ?";
	public Trip getById(int id) {
		Trip t = new Trip();
		try {
			PreparedStatement statement = connection.prepareStatement(getById);
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();		
			while (rs.next()) {				
				t.setTripId(rs.getInt("tripId"));
				t.setBookedTicket(rs.getInt("bookedTicketNumber"));
				t.setCarType(rs.getString("carType"));
				t.setDepartureDate(rs.getDate("departureDate"));
				t.setDepartureTime(rs.getTime("departureTime"));
				t.setDestination(rs.getString("destination"));
				t.setDriver(rs.getString("driver"));
				t.setMaxTicket(rs.getInt("maximumOnlineTicketNumber"));
			}
		} catch (SQLException ex) {
			Logger.getLogger(TripDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		return t;
	}
	//Search TRIP
	String search = "SELECT [tripId],[bookedTicketNumber],[carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber] "
			+ "FROM Trip WHERE destination LIKE ? OR driver LIKE ?";
	
	public ArrayList<Trip> seacrh(String searchWord) {
		ArrayList<Trip> trips = new ArrayList<Trip>();
		try {
			String search = "SELECT [tripId],[bookedTicketNumber],[carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber] "
					+ "FROM Trip WHERE destination LIKE ? OR driver LIKE ?";
			PreparedStatement statement = connection.prepareStatement(search);
			statement.setString(1,"%"+searchWord+"%");
			statement.setString(2,"%"+searchWord+"%");
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Trip t = new Trip();
				t.setTripId(rs.getInt("tripId"));
				t.setBookedTicket(rs.getInt("bookedTicketNumber"));
				t.setCarType(rs.getString("carType"));
				t.setDepartureDate(rs.getDate("departureDate"));
				t.setDepartureTime(rs.getTime("departureTime"));
				t.setDestination(rs.getString("destination"));
				t.setDriver(rs.getString("driver"));
				t.setMaxTicket(rs.getInt("maximumOnlineTicketNumber"));
				trips.add(t);
			}
		} catch (SQLException ex) {
			Logger.getLogger(TripDao.class.getName()).log(Level.SEVERE, null, ex);
		}
		return trips;
	}
	public static void main(String[] args) {
		TripDao db = new TripDao();
		ArrayList<Trip> trips = new ArrayList<Trip>();
		trips = db.getAll();
		for (Trip trip : trips) {
			System.out.println(trip.getDepartureDate());
			System.out.println(trip.getDepartureTime());
		}
	}
}
