package dao.Bookingoffice;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entity.Bookingoffice.Bookingoffice;
import utils.Bookingoffice.DBUtils;



public class BookingofficeDAO {

	public List<Bookingoffice> getList() throws Exception {
		List<Bookingoffice> list = new ArrayList<Bookingoffice>();
		Connection connection = null;
		DBUtils db = new DBUtils();
		CallableStatement callableStatement = null;
		ResultSet rs = null;
		try {
			connection = db.getConnection();
			callableStatement = connection.prepareCall("SELECT * FROM bookingoffice");
			rs = callableStatement.executeQuery();
			int param;
			Bookingoffice b = null;
			while (rs.next()) {
				param = 0;
				b = new Bookingoffice(rs.getInt(++param), rs.getDate(++param), rs.getString(++param),
						rs.getString(++param), rs.getString(++param), rs.getInt(++param), rs.getDate(++param),
						rs.getInt(++param));
				list.add(b);
			}
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db.closeConnection(connection);
		}
		return list;
	}

	public List<Bookingoffice> getListBookingoffice(int pageIndex, int pageSize, String order, String searchString)
			throws SQLException, Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		List<Bookingoffice> list = new ArrayList<Bookingoffice>();
		try {
			int begin = (pageIndex - 1) * pageSize + 1;
			int end = pageIndex * pageSize;
			String query = "SELECT * FROM(SELECT bookingoffice.officeId, bookingoffice.officeName, trip.destination, ROW_NUMBER() OVER (ORDER BY "
					+ order
					+ " ASC) AS rn FROM bookingoffice INNER JOIN trip ON bookingoffice.tripId = trip.tripId AND officeName LIKE '%"
					+ searchString + "%')AS rn WHERE rn BETWEEN " + begin + " AND " + end;
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Bookingoffice b = new Bookingoffice(rs.getInt("officeId"), rs.getString("officeName"),
						rs.getString("destination"));
				list.add(b);
			}
			return list;
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
	}

	public int getCount(String searchString) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		int count = 0;
		try {
			String query = "SELECT COUNT(officeId) FROM bookingoffice WHERE officeName LIKE '%" + searchString + "%'";
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return count;
	}

	public List<Bookingoffice> getDestination() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		List<Bookingoffice> list = new ArrayList<Bookingoffice>();
		try {
			String query = "SELECT destination FROM trip";
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Bookingoffice b = new Bookingoffice(rs.getString("destination"));
				list.add(b);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return list;
	}

	public int getTripId(String destination) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		int tripId = 0;
		try {
			String query = "SELECT tripId FROM trip WHERE destination='" + destination + "'";
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				tripId = rs.getInt(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return tripId;
	}

	public void AddBookingoffice(String endContactDeadline, String officeName, String officePhone, String officePlace,
			int officePrice, String startContactDeadline, int tripId) throws Exception {
		DBUtils db = new DBUtils();

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String sqlString = "INSERT INTO bookingoffice VALUES('" + endContactDeadline + "', '" + officeName + "', '"
				+ officePhone + "', '" + officePlace + "', " + officePrice + ", '" + startContactDeadline + "', "
				+ tripId + ")";
		try {
			con = db.getConnection();
			ps = con.prepareStatement(sqlString);
			ps.execute();
		} catch (Exception e) {
			throw e;
		} finally {
			// close Connection
			db.closeConnection(con);
		}
	}

	public Bookingoffice getBookingofficeDetail(int officeId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		Bookingoffice b = null;
		try {
			String query = "SELECT * FROM bookingoffice WHERE officeId = " + officeId;
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				b = new Bookingoffice(rs.getInt("officeId"), rs.getDate("endContactDeadline"),
						rs.getString("officeName"), rs.getString("officePhone"), rs.getString("officePlace"),
						rs.getInt("officePrice"), rs.getDate("startContactDeadline"), rs.getInt("tripId"));

			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return b;
	}

	public String getDestinationByID(int tripId) throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		String destinationString = "";
		try {
			String query = "SELECT destination FROM trip WHERE tripID = " + tripId;
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				destinationString = rs.getString(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return destinationString;
	}

	public int getMaxOfficeId() throws Exception {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DBUtils db = new DBUtils();
		int officeId = 0;
		try {
			String query = "SELECT max(officeId) FROM bookingoffice";
			conn = db.getConnection();
			ps = conn.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				officeId = rs.getInt(1);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			db.closeConnection(conn);
		}
		return officeId;
	}
}