package entity.Bookingoffice;

import java.util.Date;

public class Bookingoffice {
	private int officeId;
	private Date endContactDeadline;
	private String officeName;
	private String officePhone;
	private String officePlace;
	private int officePrice;
	private Date startContactDeadline;
	private int tripId;
	private String destination;

	public Bookingoffice() {
		super();
	}

	public Bookingoffice(String destination) {
		super();
		this.destination = destination;
	}

	public Bookingoffice(int officeId, String officeName, String destination) {
		super();
		this.officeId = officeId;
		this.officeName = officeName;
		this.destination = destination;
	}

	public Bookingoffice(int officeId, Date endContactDeadline, String officeName, String officePhone,
			String officePlace, int officePrice, Date startContactDeadline, int tripId) {
		super();
		this.officeId = officeId;
		this.endContactDeadline = endContactDeadline;
		this.officeName = officeName;
		this.officePhone = officePhone;
		this.officePlace = officePlace;
		this.officePrice = officePrice;
		this.startContactDeadline = startContactDeadline;
		this.tripId = tripId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getOfficeId() {
		return officeId;
	}

	public void setOfficeId(int officeId) {
		this.officeId = officeId;
	}

	public Date getEndContactDeadline() {
		return endContactDeadline;
	}

	public void setEndContactDeadline(Date endContactDeadline) {
		this.endContactDeadline = endContactDeadline;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getOfficePlace() {
		return officePlace;
	}

	public void setOfficePlace(String officePlace) {
		this.officePlace = officePlace;
	}

	public int getOfficePrice() {
		return officePrice;
	}

	public void setOfficePrice(int officePrice) {
		this.officePrice = officePrice;
	}

	public Date getStartContactDeadline() {
		return startContactDeadline;
	}

	public void setStartContactDeadline(Date startContactDeadline) {
		this.startContactDeadline = startContactDeadline;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
}