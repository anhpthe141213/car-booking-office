package controller.Bookingoffice;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Bookingoffice.BookingofficeDAO;
import entity.Bookingoffice.Bookingoffice;



/**
 * Servlet implementation class ListBookingofficeServlet
 */
@WebServlet("/ListBookingofficeServlet")
public class ListBookingofficeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String order = "officeName";
		String searchString = "";
		int pageIndex = 1;
		int pageSize = 3;
		int total = 1;

		if (request.getParameter("order") != null) {
			order = request.getParameter("order");
		}

		if (request.getParameter("searchString") != null) {
			searchString = request.getParameter("searchString");
		}

		BookingofficeDAO bd = new BookingofficeDAO();
		List<Bookingoffice> list = new ArrayList<Bookingoffice>();

		if (request.getParameter("pageIndex") != null) {
			pageIndex = Integer.parseInt(request.getParameter("pageIndex"));
		} else {
			pageIndex = 1;
		}

		try {
			total = bd.getCount(searchString);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		int maxPage = total / pageSize;

		if (total % pageSize != 0) {
			maxPage++;
		}

		if (pageIndex <= 0 || pageIndex > maxPage) {
			pageIndex = 1;
		}

		try {
			list = bd.getListBookingoffice(pageIndex, pageSize, order, searchString);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PrintWriter out = response.getWriter();

		if (list.size() > 0) {
			out.println("<div class=\"table-responsive pt-15\">\r\n"
					+ "					<table class=\"table table-hover table-striped table-bordered\">\r\n"
					+ "						<thead class=\"table-dark\">\r\n"
					+ "							<tr>\r\n"
					+ "								<th scope=\"col\">ID</th>\r\n"
					+ "								<th scope=\"col\">Booking office</th>\r\n"
					+ "								<th scope=\"col\">Trip</th>\r\n"
					+ "								<th scope=\"col\">Action</th>\r\n"
					+ "							</tr>\r\n"
					+ "						</thead>\r\n"
					+ "						<tbody>");

			for (Bookingoffice b : list) {
				out.println("<tr>\r\n"
						+ "								<td scope=\"row\">" + b.getOfficeId() + "</td>\r\n"
						+ "								<td scope=\"row\"><a href=\"CallOfficeDetailPageServlet?officeId=" + b.getOfficeId() + "\">" + b.getOfficeName() + "</a></td>\r\n"
						+ "								<td scope=\"row\">" + b.getDestination() + "</td>\r\n"
						+ "								<td scope=\"row\"><a href=\"CallLBookingDetailPageServlet?officeId=" + b.getOfficeId() +"\"><i class=\"fa fa-eye\"></i>\r\n"
						+ "										View</a></td>\r\n"
						+ "							</tr>");

			}
//			<tr>\r\n" + "		<td>" + b.getOfficeId() + "</td>\r\n"
//			+ "		<td><a href=\"CallOfficeDetailPageServlet?officeId=" + b.getOfficeId()
//			+ "\" style=\"color: #3366CC; text-decoration: none\">" + b.getOfficeName() + "</a></td>\r\n"
//			+ "		<td>" + b.getDestination() + "</td>\r\n" + "		<td><a\r\n"
//			+ "			href=\"CallLBookingDetailPageServlet?officeId=" + b.getOfficeId()
//			+ "\" style=\"color: #3366CC; text-decoration: none\">View</a></td>\r\n" + "	</tr>\r\n
			out.println("</tbody>\r\n"
					+ "					</table>\r\n"
					+ "				</div>\r\n"
					+ "				<nav>\r\n"
					+ "					<ul class=\"pagination\">");



			int x = pageIndex - 1;

			out.println("<li class=\"page-item\"><a class=\"page-link\" onclick=\"loadd(" + x + ")\">Previous</a></li>");

			for (int i = 1; i <= maxPage; i++) {
				if (i == pageIndex) {
					out.println("<li class=\"page-item active\"><a class=\"page-link\" onclick=\"loadd(" + i + ")\">" + i + "</a></li>");
				} else {
					out.println("<li class=\"page-item\"><a class=\"page-link\" onclick=\"loadd(" + i + ")\">" + i + "</a></li>");
				}
			}

			int y = pageIndex + 1;
			if (y == maxPage + 1) {
				y = maxPage;
			}

			out.println("<li class=\"page-item\"><a class=\"page-link\" onclick=\"loadd(" + y + ")\">Next</a></li>");

			out.println("</ul>\r\n"
					+ "				</nav>");
		} else {
			
			out.println("<p style=\"color: red\">No Match !</p>");
			out.println("<div class=\"backHome\">");
			out.println(" <button onclick=\"home()\">List</button>");
			out.println("</div>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

	}

}