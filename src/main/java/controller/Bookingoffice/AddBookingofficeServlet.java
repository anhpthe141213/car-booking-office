package controller.Bookingoffice;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Bookingoffice.BookingofficeDAO;


/**
 * Servlet implementation class CreateContentServlet
 */
@WebServlet("/AddBookingofficeServlet")
public class AddBookingofficeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String endContactDeadline = request.getParameter("toDate");
		String officeName = request.getParameter("officeName");
		String officePhone = request.getParameter("officePhone");
		String officePlace = request.getParameter("fiter_option");
		String destination = request.getParameter("destination");
		int officePrice = Integer.parseInt(request.getParameter("officePrice"));
		String startContactDeadline = request.getParameter("startDate");
		
		int tripId = 1;
		
		
		boolean isofficeNameOK = officeName != null && officeName.trim().length() > 0 && officeName.trim().length() <= 50;
		boolean isofficePhonedOk = officePhone != null && officePhone.trim().length() == 11;
		int officeId = 1;
		
		BookingofficeDAO bd = new BookingofficeDAO();
		if (isofficeNameOK && isofficePhonedOk ) {
			try {
				
				tripId = bd.getTripId(destination);
				bd.AddBookingoffice(endContactDeadline, officeName, officePhone, officePlace, officePrice, startContactDeadline, tripId);
				officeId = bd.getMaxOfficeId();
			} catch (Exception e) {
				
			}
		} else {
			
		}
		request.setAttribute("officeId", officeId);
		request.getRequestDispatcher("CallLBookingDetailPageServlet?officeId=" + officeId).forward(request, response);
		doGet(request, response);
	}

}
