package controller.Bookingoffice;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Bookingoffice.BookingofficeDAO;
import entity.Bookingoffice.Bookingoffice;





/**
 * Servlet implementation class ContentDetailServlet
 */
@WebServlet("/CallOfficeDetailPageServlet")
public class CallOfficeDetailPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int officeId = Integer.parseInt(request.getParameter("officeId"));
		Bookingoffice b = new Bookingoffice();
		BookingofficeDAO bd = new BookingofficeDAO();
		try {
			b = bd.getBookingofficeDetail(officeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("officeId", officeId);
		request.setAttribute("officeName", b.getOfficeName());
		request.setAttribute("officePhone", b.getOfficePhone());
		request.setAttribute("officePlace", b.getOfficePlace());
		request.setAttribute("officePrice", b.getOfficePrice());
		request.getRequestDispatcher("/views/office_detail.jsp").forward(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
