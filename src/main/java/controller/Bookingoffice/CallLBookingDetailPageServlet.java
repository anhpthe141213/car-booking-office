package controller.Bookingoffice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.Bookingoffice.BookingofficeDAO;
import entity.Bookingoffice.Bookingoffice;



/**
 * Servlet implementation class ContentDetailServlet
 */
@WebServlet("/CallLBookingDetailPageServlet")
public class CallLBookingDetailPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int officeId = Integer.parseInt(request.getParameter("officeId"));
		Bookingoffice b = new Bookingoffice();
		BookingofficeDAO bd = new BookingofficeDAO();
		String destination = "";
		try {
			b = bd.getBookingofficeDetail(officeId);
			destination = bd.getDestinationByID(b.getTripId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("officeId", officeId);
		request.setAttribute("endContactDeadline", b.getEndContactDeadline());
		request.setAttribute("officeName", b.getOfficeName());
		request.setAttribute("officePhone", b.getOfficePhone());
		request.setAttribute("officePlace", b.getOfficePlace());
		request.setAttribute("officePrice", b.getOfficePrice());
		request.setAttribute("startContactDeadline", b.getStartContactDeadline());
		request.setAttribute("tripId", b.getTripId());
		request.setAttribute("destination", destination);
		request.getRequestDispatcher("/views/booking_detail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
