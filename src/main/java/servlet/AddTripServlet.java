package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.TripDao;
/**
 * Servlet implementation class AddTripServlet
 */
public class AddTripServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		TripDao db = new TripDao();
		String destination = request.getParameter("destination");
		String time = request.getParameter("departureTime");
		String driver = request.getParameter("driver");
		String carType = request.getParameter("carType");
		String maxTicket = request.getParameter("maxTicket");
		String departureDate = request.getParameter("departureDate");
		
		try {
			//String carType, String departureDate, String departureTime, String destination, String driver, int maxTicket
			db.create(carType, departureDate, time, destination, driver, Integer.parseInt(maxTicket));
			response.sendRedirect("TripList.jsp");
		} catch (SQLException ex) {
			//System.out.println(time+"  "+departureDate+"  "+maxTicket);
			response.getWriter().println("Error");
			ex.printStackTrace();
		}
	}

}
