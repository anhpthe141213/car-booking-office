package servlet;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TripDao;
import entities.Trip;

/**
 * Servlet implementation class UpdateTripServlet
 */
public class UpdateTripServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TripDao db = new TripDao();
		String id = request.getParameter("id");
		Trip trip = db.getById(Integer.parseInt(id));

		String destination = trip.getDestination();
		Time time = (Time) trip.getDepartureTime();
		String driver = trip.getDriver();
		String carType = trip.getCarType();
		int maxTicket = trip.getMaxTicket();
		Date departureDate = (Date) trip.getDepartureDate();

		request.setAttribute("destination", destination);
		request.setAttribute("departureTime", time);
		request.setAttribute("driver", driver);
		request.setAttribute("carType", carType);
		request.setAttribute("maxTicket", maxTicket);
		request.setAttribute("departureDate", departureDate);
		request.setAttribute("id", id);

		request.getRequestDispatcher("UpdateTrip.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TripDao db = new TripDao();
		String destination = request.getParameter("destination");
		String time = request.getParameter("departureTime");
		String driver = request.getParameter("driver");
		String carType = request.getParameter("carType");
		String maxTicket = request.getParameter("maxTicket");
		String departureDate = request.getParameter("departureDate");

		try {
			// String carType, String departureDate, String departureTime, String
			// destination, String driver, int maxTicket
			String id = request.getParameter("id");
			db.update(carType, departureDate, time, destination, driver, Integer.parseInt(maxTicket),
					Integer.parseInt(id));
			response.sendRedirect("TripList.jsp");
		} catch (SQLException ex) {
			// System.out.println(time+" "+departureDate+" "+maxTicket);
			response.getWriter().println("Error");
			ex.printStackTrace();
		}
	}

}
