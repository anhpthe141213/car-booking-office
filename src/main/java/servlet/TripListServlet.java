package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.TripDao;
import entities.Trip;

/**
 * Servlet implementation class TripListServlet
 */
public class TripListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		TripDao db = new TripDao();
		ArrayList<Trip> trips = new ArrayList<Trip>();
		trips = db.getAll();
		for (Trip s : trips) {
			out.print("<tr>\r\n"
					+ "						<td>"+s.getTripId()+"</td>\r\n"
					+ "						<td>"+s.getDestination()+"</td>\r\n"
					+ "						<td>"+s.getDepartureTime()+"</td>\r\n"
					+ "						<td>"+s.getDriver()+"</td>\r\n"
					+ "						<td>"+s.getCarType()+"</td>\r\n"
					+ "						<td>"+s.getBookedTicket()+"</td>\r\n"
					+ "						<td><a href =\"UpdateTripServlet?id="+s.getTripId()+"\">Update</a> <a href =\"DeleteTripServlet?id="+s.getTripId()+"\" onclick=\"confirmDelete()\">Delete</a></td>\r\n" 
					+ "					</tr>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
