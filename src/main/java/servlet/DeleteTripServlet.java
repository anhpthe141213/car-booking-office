package servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.TripDao;
/**
 * Servlet implementation class DeleteTripServlet
 */
public class DeleteTripServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		TripDao db = new TripDao();
		String id = request.getParameter("id");
		try {
			//String carType, String departureDate, String departureTime, String destination, String driver, int maxTicket
			db.delete(Integer.parseInt(id));
			response.sendRedirect("TripList.jsp");
		} catch (SQLException ex) {
			response.getWriter().println("Error");
			ex.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
