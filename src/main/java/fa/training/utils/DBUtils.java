package fa.training.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.chainsaw.Main;

public class DBUtils {
	private static DBUtils instance;
    private static Connection connection;

    public static DBUtils getInstance() {
        if (instance == null || connection == null) {
            instance = new DBUtils();
        }
        return instance;
    }

    public static Connection getConnection() {
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433; databaseName =" + Constants.DB_NAME;

            connection = DriverManager.getConnection(url, Constants.USERNAME, Constants.PASSWORD);
            return connection;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }catch(SQLException ex) {
        	ex.printStackTrace();
        }
        return null;
    }

	public static void closeConnection(Connection connection2) {
		try {
			connection2.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}