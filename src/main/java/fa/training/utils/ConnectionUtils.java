package fa.training.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
	public static Connection getConnection() throws ClassNotFoundException, SQLException {

    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

    // Connect to DB
    String url = "jdbc:sqlserver://localhost:1433;databaseName=mock";
    Connection connection = DriverManager.getConnection(url, "sa", "12345");
    return connection;
  }

}
