/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 26 thg 7, 2021
 *	@version 1.8
 */
package fa.training.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.ParkingLotDaoImp;
import fa.training.model.ParkingLot;

@WebServlet(urlPatterns = "/search")
public class SearchController extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("views/parking-lot-list.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String searchString = req.getParameter("searchString").trim();
		String filterBy = req.getParameter("filterBy").trim();
		ParkingLotDaoImp pdb = new ParkingLotDaoImp();
		List<ParkingLot> list = pdb.getAllParkingLot(searchString, filterBy);
		if(list.isEmpty()) {
			PrintWriter out = resp.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Not found');");
			out.println("location='views/parking-lot-list.jsp'");
			out.println("</script>");
		}else {
			req.setAttribute("list", list);
			req.getRequestDispatcher("views/parking-lot-list.jsp").forward(req, resp);
		}
	}
}