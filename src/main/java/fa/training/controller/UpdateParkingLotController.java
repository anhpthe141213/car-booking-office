/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 23 thg 7, 2021
 *	@version 1.8
 */
package fa.training.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.ParkingLotDaoImp;
import fa.training.model.ParkingLot;

@WebServlet(urlPatterns = "/update")
public class UpdateParkingLotController extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("views/parking-lot-list.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		ParkingLot p = (ParkingLot)session.getAttribute("parkinglot");
		p.setParking_lot_name(req.getParameter("parkingLotName").trim());
		int place = Integer.parseInt(req.getParameter("place"));
		switch(place) {
		case 1:{
			p.setPlace("Khu Dong");
			break;
		}
		case 2:{
			p.setPlace("Khu Tay");
			break;
		}
		case 3:{
			p.setPlace("Khu Nam");
			break;
		}
		case 4:{
			p.setPlace("Khu Bac");
			break;
		}
		}
		p.setArea(Integer.parseInt(req.getParameter("area").trim()));
		p.setPrice(Integer.parseInt(req.getParameter("price").trim()));
		ParkingLotDaoImp pdb = new ParkingLotDaoImp();
		pdb.updateParkingLot(p);
		PrintWriter out = resp.getWriter();
		out.println("<script type=\"text/javascript\">");
		out.println("alert('Update successful!');");
		out.println("location='"+req.getContextPath() + "/ParkingLotList';");
		out.println("</script>");
	}
}