package fa.training.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.ParkingLotDaoImp;
import fa.training.model.ParkingLot;

/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 22 thg 7, 2021
 *	@version 1.8
 */

@WebServlet(urlPatterns = "/ParkingLotList")

public class ShowParkingLotListController extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		ParkingLotDaoImp pdb = new ParkingLotDaoImp();
		List<ParkingLot> list = pdb.getAllParkingLot("", "");
		req.setAttribute("list", list);
		req.getRequestDispatcher("views/parking-lot-list.jsp").forward(req, resp);
	}
}