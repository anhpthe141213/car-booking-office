package fa.training.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.EmployeeDAO;
import fa.training.entities.Employee;

/**
 * Servlet implementation class DetailEmployeeController
 */
@WebServlet("/DetailEmployeeController")
public class DetailEmployeeController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailEmployeeController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int employeeId = Integer.parseInt(request.getParameter("employeeId"));
		Employee e = new Employee();
		EmployeeDAO dao = new EmployeeDAO();
		try {
			e = dao.getEmployeeDetail(employeeId);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		request.setAttribute("employeeId", employeeId);
		request.setAttribute("account", e.getAccount());
		request.setAttribute("department", e.getDepartment());
		request.setAttribute("employeeAddress", e.getEmployeeAddress());
		request.setAttribute("employeeBirthdate", e.getEmployeeBirthdate());
		request.setAttribute("employeeEmail", e.getEmployeeEmail());
		request.setAttribute("employeeName", e.getEmployeeName());
		request.setAttribute("employeePhone", e.getEmployeePhone());
		request.setAttribute("password", e.getPassword());
		request.setAttribute("sex", e.getSex());
		request.getRequestDispatcher("views/employee-detail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
