/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 23 thg 7, 2021
 *	@version 1.8
 */
package fa.training.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fa.training.dao.ParkingLotDaoImp;
import fa.training.model.ParkingLot;

@WebServlet(urlPatterns = "/actionController")
public class ActionController extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String action = req.getParameter("action");
		int id = Integer.parseInt(req.getParameter("id"));
		ParkingLotDaoImp pld = new ParkingLotDaoImp();
		ParkingLot p = pld.getParkingLotById(id);
		if (action.equals("edit")) {
			HttpSession session = req.getSession();
			session.setAttribute("parkinglot", p);
			req.getRequestDispatcher("views/parking-lot-edit.jsp").forward(req, resp);
		} else if(action.equals("delete")){
			pld.deleteParkingLotById(p);
			PrintWriter out = resp.getWriter();
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Delete successful!');");
			out.println("location='"+req.getContextPath() + "/ParkingLotList';");
			out.println("</script>");
		}
	}
}