package fa.training.controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fa.training.dao.EmployeeDAO;
import fa.training.entities.Employee;

/**
 * Servlet implementation class AddController
 */
@WebServlet("/AddEmployeeController")
public class AddEmployeeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployeeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EmployeeDAO employeeDAO = new EmployeeDAO();
		try {
			ArrayList<Employee> listEmployee = employeeDAO.listEmployee();
			request.setAttribute("listEmployee", listEmployee);
			request.getRequestDispatcher("views/employee-add.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String fullName = request.getParameter("fullname");
		String phone = request.getParameter("phonenumber");
		String birth = request.getParameter("dateofbirth");
		String sex = request.getParameter("gender");
		String address = request.getParameter("address");
		String email = request.getParameter("email");
		String account = request.getParameter("account");
		String pass = request.getParameter("pass");
		String deparment = request.getParameter("department");
		
		Employee employee = new Employee(account, deparment, address, birth, email, fullName, phone, pass, sex);
		EmployeeDAO dao = new EmployeeDAO();
		try {
			dao.addEmployee(employee);
			response.sendRedirect(request.getContextPath() + "/ListEmployeeController");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
