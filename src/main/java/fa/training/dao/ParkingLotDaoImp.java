/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 22 thg 7, 2021
 *	@version 1.8
 */
package fa.training.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import fa.training.model.ParkingLot;
import fa.training.utils.DBUtils;

public class ParkingLotDaoImp implements ParkingLotDao {

	public List<ParkingLot> getAllParkingLot(String searchString, String filterby) {
		List<ParkingLot> list = new ArrayList<ParkingLot>();
		Connection connect = DBUtils.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select * from parkinglot ";
			if (!searchString.equals("")) {
				sql += "where " + filterby + " like'%"+searchString+"%'";
			} 
			ps = connect.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				ParkingLot p = new ParkingLot();
				p.setPark_id(rs.getInt(1));
				p.setArea(rs.getInt(2));
				p.setParking_lot_name(rs.getString(3));
				p.setPlace(rs.getString(4));
				p.setPrice(rs.getInt(5));
				p.setStatus(rs.getString(6));
				list.add(p);
			}
			rs.close();
			ps.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public void addParkingLot(ParkingLot p) {
		Connection connect = DBUtils.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "insert into parkinglot(parkName, parkPlace, parkArea, parkPrice, parkStatus) values(?,?,?,?,'Blank')";
			ps = connect.prepareStatement(sql);
			ps.setString(1, p.getParking_lot_name());
			ps.setString(2, p.getPlace());
			ps.setInt(3, p.getArea());
			ps.setInt(4, p.getPrice());
			ps.execute();
			ps.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateParkingLot(ParkingLot p) {
		Connection connect = DBUtils.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "update parkinglot set parkName=?, parkPlace=?, parkArea=?, parkPrice=? where parkId=?";
			ps = connect.prepareStatement(sql);
			ps.setString(1, p.getParking_lot_name());
			ps.setString(2, p.getPlace());
			ps.setInt(3, p.getArea());
			ps.setInt(4, p.getPrice());
			ps.setInt(5, p.getPark_id());
			ps.execute();
			ps.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public ParkingLot getParkingLotById(int id) {
		ParkingLot p = new ParkingLot();
		Connection connect = DBUtils.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String sql = "select * from parkinglot where parkId = ?";
			ps = connect.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				p.setPark_id(rs.getInt(1));
				p.setArea(rs.getInt(2));
				p.setParking_lot_name(rs.getString(3));
				p.setPlace(rs.getString(4));
				p.setPrice(rs.getInt(5));
				p.setStatus(rs.getString(6));
			}
			rs.close();
			ps.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return p;
	}

	
	public void deleteParkingLotById(ParkingLot p) {
		Connection connect = DBUtils.getConnection();
		PreparedStatement ps = null;
		try {
			String sql = "delete from parkinglot where parkId=?";
			ps = connect.prepareStatement(sql);
			ps.setInt(1, p.getPark_id());
			ps.execute();
			ps.close();
			connect.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}