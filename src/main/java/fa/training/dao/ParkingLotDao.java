/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 22 thg 7, 2021
 *	@version 1.8
 */
package fa.training.dao;

import java.util.List;

import fa.training.model.ParkingLot;

public interface ParkingLotDao {
	List<ParkingLot> getAllParkingLot(String searchString, String filterby);
	ParkingLot getParkingLotById(int id);
	void addParkingLot(ParkingLot p);
	void updateParkingLot(ParkingLot p);
	void deleteParkingLotById(ParkingLot p);
}