package fa.training.dao;

import java.sql.Connection;    
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import fa.training.entities.Employee;
import fa.training.utils.ConnectionUtils;

public class EmployeeDAO {
	
	
	public ArrayList<Employee> listEmployee() throws Exception {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Employee> Employees = new ArrayList<Employee>();
		try {
			String query = "SELECT [employeeid]\r\n"
					+ "	,[employeeName]\r\n"
					+ "	,[employeeBirthdate]\r\n"
					+ "	,[employeeAddress]\r\n"
					+ "	,[employeePhone]\r\n"
					+ "    ,[department]\r\n"
					+ "  FROM [dbo].[employee]";
			connection = ConnectionUtils.getConnection();
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				Employee e = new Employee();
				e.setEmployeeId(rs.getInt(1));
				e.setEmployeeName(rs.getString(2));
				e.setEmployeeBirthdate(rs.getString(3));
				e.setEmployeeAddress(rs.getString(4));
				e.setEmployeePhone(rs.getString(5));
				e.setDepartment(rs.getString(6));
				
				Employees.add(e);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
		return Employees;
	}
	
	public void deleteEmployee(int id) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = (Connection) ConnectionUtils.getConnection();

			preparedStatement = connection.prepareStatement("DELETE FROM [dbo].[employee]\r\n"
					+ "      WHERE employeeid = ?");

			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
		} catch (Exception exception) {
			exception.printStackTrace();

			throw exception;

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}
	
	public void updateEmployee(Employee employee) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = ConnectionUtils.getConnection();

			preparedStatement = connection
					.prepareStatement("UPDATE [dbo].[employee]\r\n"
							+ "   SET [account] = ?\r\n"
							+ "      ,[department] = ?\r\n"
							+ "      ,[employeeAddress] = ?\r\n"
							+ "      ,[employeeBirthdate] = ?\r\n"
							+ "      ,[employeeEmail] = ?\r\n"
							+ "      ,[employeeName] = ?\r\n"
							+ "      ,[employeePhone] = ?\r\n"
							+ "      ,[password] = ?\r\n"
							+ "      ,[sex] = ?\r\n"
							+ " WHERE employeeid = ?");
			
			preparedStatement.setString(1, employee.getAccount());
			preparedStatement.setString(2, employee.getDepartment());
			preparedStatement.setString(3, employee.getEmployeeAddress());
			preparedStatement.setString(4, employee.getEmployeeBirthdate());
			preparedStatement.setString(5, employee.getEmployeeEmail());
			preparedStatement.setString(6, employee.getEmployeeName());
			preparedStatement.setString(7, employee.getEmployeePhone());
			preparedStatement.setString(8, employee.getPassword());
			preparedStatement.setString(9, employee.getSex());
			preparedStatement.setInt(10, employee.getEmployeeId());
			
			preparedStatement.executeUpdate();
		} catch (Exception exception) {
			exception.printStackTrace();

			throw exception;

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}
	

	public void addEmployee(Employee employee) throws Exception {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		try {
			connection = (Connection) ConnectionUtils.getConnection();

			preparedStatement = connection.prepareStatement("INSERT INTO [dbo].[employee]\r\n"
					+ "           ([employeeName]\r\n"
					+ "		   ,[employeePhone]\r\n"
					+ "		   ,[employeeBirthdate]\r\n"
					+ "		   ,[sex]\r\n"
					+ "		   ,[employeeAddress]\r\n"
					+ "		   ,[employeeEmail]\r\n"
					+ "		   ,[account]\r\n"
					+ "		   ,[password]\r\n"
					+ "           ,[department])\r\n"
					+ "     VALUES\r\n"
					+ "           (?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?\r\n"
					+ "           ,?)");

			preparedStatement.setString(1, employee.getEmployeeName());
			preparedStatement.setString(2, employee.getEmployeePhone());
			preparedStatement.setString(3, employee.getEmployeeBirthdate());
			preparedStatement.setString(4, employee.getSex());
			preparedStatement.setString(5, employee.getEmployeeAddress());
			preparedStatement.setString(6, employee.getEmployeeEmail());
			preparedStatement.setString(7, employee.getAccount());
			preparedStatement.setString(8, employee.getPassword());
			preparedStatement.setString(9, employee.getDepartment());
			

			preparedStatement.executeUpdate();
		} catch (Exception exception) {
			exception.printStackTrace();

			throw exception;

		} finally {
			if (connection != null) {
				connection.close();
			}
		}
	}
	
	public Employee getEmployeeDetail(int employeeId) throws Exception {
		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Employee e = null;
		try {
			String query = "SELECT * FROM employee \r\n"
					+ "WHERE employeeid =" + employeeId;
			connection = ConnectionUtils.getConnection();
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				e = new Employee();
				e.setEmployeeId(rs.getInt(1));
				e.setAccount(rs.getString(2));
				e.setDepartment(rs.getString(3));
				e.setEmployeeAddress(rs.getString(4));
				e.setEmployeeBirthdate(rs.getString(5));
				e.setEmployeeEmail(rs.getString(6));
				e.setEmployeeName(rs.getString(7));
				e.setEmployeePhone(rs.getString(8));
				e.setPassword(rs.getString(9));
				e.setSex(rs.getString(10));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			if (connection != null) {
				connection.close();
			}
		}
		return e;
	}
	
	public ArrayList<Employee> getListByPage(ArrayList<Employee> list,
            int start,int end){
        ArrayList<Employee> arr=new ArrayList<Employee>();
        for(int i=start;i<end;i++){
            arr.add(list.get(i));
        }
        return arr;
    }

	
	public static void main(String[] args) throws Exception {
		EmployeeDAO dao = new EmployeeDAO();
		System.out.println(dao.getEmployeeDetail(1));
//		Employee e = new Employee("dat4","employee","Nam Dan","2000-11-02","dat4@gmail.com","Hoang Tien Dat","0964697689","6666","1");
//		dao.addEmployee(e);
	}
}
