/*
 * (C) Copyright 2021 Fresher Academy. All Rights Reserved.
 * 
 *	@author tuan nguyen van
 *	@date 22 thg 7, 2021
 *	@version 1.8
 */
package fa.training.model;

public class ParkingLot {
	private int park_id;
	private String parking_lot_name;
	private String place;
	private int area;
	private int price;
	private String status;
	public ParkingLot() {
		
	}
	public ParkingLot(String parking_lot_name, String place, int area, int price) {
		super();
		this.parking_lot_name = parking_lot_name;
		this.place = place;
		this.area = area;
		this.price = price;
	}
	public ParkingLot(int park_id, String parking_lot_name, String place, int area, int price) {
		super();
		this.park_id = park_id;
		this.parking_lot_name = parking_lot_name;
		this.place = place;
		this.area = area;
		this.price = price;
	}

	public ParkingLot(int park_id, String parking_lot_name, String place, int area, int price, String status) {
		super();
		this.park_id = park_id;
		this.parking_lot_name = parking_lot_name;
		this.place = place;
		this.area = area;
		this.price = price;
		this.status = status;
	}
	public int getPark_id() {
		return park_id;
	}
	public void setPark_id(int park_id) {
		this.park_id = park_id;
	}
	public String getParking_lot_name() {
		return parking_lot_name;
	}
	public void setParking_lot_name(String parking_lot_name) {
		this.parking_lot_name = parking_lot_name;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public int getArea() {
		return area;
	}
	public void setArea(int area) {
		this.area = area;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ParkingLot [park_id=" + park_id + ", parking_lot_name=" + parking_lot_name + ", place=" + place
				+ ", area=" + area + ", price=" + price + ", status=" + status + "]";
	}
	
}