CREATE DATABASE Mockproject
GO
USE Mockproject
GO
CREATE TABLE Trip(
[tripId] BIGINT PRIMARY KEY IDENTITY(1,1),
[bookedTicketNumber] INT DEFAULT 0,
[carType] VARCHAR(11) NOT NULL,
[departureDate] DATE NOT NULL,
[departureTime] TIME NOT NULL,
[destination] VARCHAR(50),
[driver] VARCHAR(11) NOT NULL,
[maximumOnlineTicketNumber] INT NOT NULL
)
GO
SELECT [tripId],[bookedTicketNumber],[carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber] FROM Trip WHERE destination LIKE '%%' OR driver LIKE '%%'
GO
INSERT INTO Trip ([carType],[departureDate],[departureTime],[destination],[driver],[maximumOnlineTicketNumber]) 
VALUES ('car100','2015-12-17','15:30','Hai Phong','Thi D',50)
select * from Trip
GO
DELETE FROM Trip WHERE [tripId] = 13

UPDATE Trip SET [carType] = 'Limo',[departureDate] = '2021-12-17',[departureTime] = '20:20',[destination] ='Phu Tho',[driver]='Anh C',[maximumOnlineTicketNumber] = 55 WHERE [tripId] = 1

GO

CREATE TABLE Ticket(
[ticketId] BIGINT PRIMARY KEY IDENTITY(1,1),
[bookingTime] TIME NOT NULL,
[custumerName] VARCHAR(11) NOT NULL,
[licenscePlate] VARCHAR(11) NOT NULL CHECK (DATALENGTH([licenscePlate])=11),
[tripId] BIGINT,
FOREIGN KEY ([tripId]) REFERENCES Trip([tripId])
)